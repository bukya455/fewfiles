# API helper functions
import re
# List of all types of categories which we need to check in our responses:
promotional_message = ["merchant", "offer_description", "call_to_action", "validity_date", "time"]
delivery_details_message = ["delivery_agent_name", "tracking_number", "shipment_status",
                            "estimated_delivery_date", "delivery_instructions", "date", "time"]
investment_details_message = ["merchant", "investment_type", "transaction_amount", "bank_balance", "platform"]
transactional_messages = ["bank_name", "transaction_amount", "bank_balance", "income_or_spend",
                          "debited_acc","credited_acc","currency","account_last_4_digit",
                          "merchant", "mode_of_transaction", "medium_of_transaction","category",
                          "date", "time"]
bill_remainders_message = ["merchant", "bill_type", "total_bill_amount", "min_bill_amount", "due_date", "call_to_action", "date", "time"]

# Message Classification
all_labels = ['bill_remainders', 'delivery_messages', 'investment_messages',
              'otp_messages', 'personal_messages', 'promotional_messages',
              'spam_messages', 'transactional_messages']

# Define a mapping of classification responses to allowed categories, map categories to message types
response_to_categories = {
    'transactional_messages': transactional_messages,
    'promotional_messages': promotional_message,
    'investment_messages': investment_details_message,
    'bill_remainders': bill_remainders_message,
    'delivery_messages': delivery_details_message
}

# ..................................... Date and Time ...................... #

# new format
def extract_received_date_time(text):
    final_dt =[]
    dict1 = {}
    #text = text.split("|")[1].strip()
    # Define the regex pattern to match date and time
    pattern = r'(\d{1,2}/\d{1,2}/\d{2,4})t(\d{1,2}):(\d{1,2}):(\d{1,2})'
    # Use re.search() to find the first match in the text
    match = re.search(pattern, text.lower())
    if match:
        # Extract date and time components from the match
        date = match.group(1)

        hour = match.group(2).zfill(2)  # Ensure leading zero for single-digit hours
        minute = match.group(3).zfill(2)  # Ensure leading zero for single-digit minutes
        second = match.group(4).zfill(2)  # Ensure leading zero for single-digit seconds
        # only once
        if len(final_dt)<1:
            #dict1 = {"date": date, "time": f"{hour}:{minute}:{second}"}
            final_dt.append({"received_date": date})
            final_dt.append({"received_time": f"{hour}:{minute}:{second}"})
    else:
        if len(final_dt)<1:
            #dict1 = {"date": date, "time": f"{hour}:{minute}:{second}"}
            final_dt.append({"received_date": "NA"})
            final_dt.append({"received_time": "NA"})
    
    return final_dt

def add_missing_entities(response_dict, category, entity_keys):
    # for missing entities show NA
    extracted_category = response_dict.get("Extracted Category", [])
    extracted_entities = response_dict.get("Extracted Entities", [])

    print(extracted_category, extracted_entities, "post process") 

    if extracted_category and extracted_category == category:
      ner_response = extracted_entities
      for key in entity_keys:
        if not any(key in item for item in ner_response):
          ner_response.append({key: "NA"})
    return response_dict

def filter_entities(entities, allowed_categories):
    # Ensure entities is a list of dictionaries
    if not isinstance(entities, list):
        return []
    filtered_entities = [entity for entity in entities if list(entity.keys())[0] in allowed_categories]
    return filtered_entities

# add "NA" and "others" for missing information
def add_missing_entities2(response_dict, category, entity_keys):
    others_keys = ["bank_name","merchant","mode_of_transaction", "medium_of_transaction","category",
                   "bill_type"]
    # for missing entities show NA
    extracted_category = response_dict.get("Extracted Category", [])
    extracted_entities = response_dict.get("Extracted Entities", [])

    if extracted_category and extracted_category == category:
        ner_response = extracted_entities
        for key in entity_keys:
            if not any(key in item for item in ner_response):
                if key in others_keys:
                    ner_response.append({key: "Others"})
                else:
                    ner_response.append({key: "NA"})
    return response_dict

# list of dicts to one dictionary
def listdicts_to_dict(list_of_dicts):
  result_dict = {}
  for item in list_of_dicts:
    for key, value in item.items():
        if key in result_dict:
            if isinstance(result_dict[key], list):
                result_dict[key].append(value)
            else:
                result_dict[key] = [result_dict[key], value]
        else:
            result_dict[key] = value
            
  return result_dict

def category_ner_entities(text, classification_response, ner_results):
  try:
      # map extracted entities and for not found 'NA'
    response_dict = {}
    # Check if the classification_response is in the mapping
    if classification_response in response_to_categories and isinstance(ner_results, list):
      allowed_categories = response_to_categories[classification_response]
      # Ensure ner_results[0] is a list of dictionaries
      filtered_entities = filter_entities(ner_results, allowed_categories)
      received_date_time = extract_received_date_time(text)
      ner_results = filtered_entities+received_date_time

      response_dict = {"Input Text": text, "Extracted Category":classification_response, 
                       "Extracted Entities": ner_results}
      
      # Loop through the categories and add missing entities
      for category, message_types in response_to_categories.items():
        response_dict = add_missing_entities2(response_dict, category, message_types) # add missing information as "NA" and "others"

    response_dict['Extracted Entities'] = listdicts_to_dict(response_dict['Extracted Entities'])
    return response_dict # response_dict

  except:
      # category found but no entities found
      response_dict = {"Input Text": text, "Extracted Category":classification_response, 
                       "Extracted Entities": "No Entities Found!"}
      return response_dict
      
      
