# list of entities with categorries

mode_of_transactions = ['Cash ','Cheques', 'Debit card', 'Credit card', 'Wallet', 'UPI', 'NEFT', 'RTGS', 'IMPS', 'AEPS', 'ATM']

medium_of_transactions = ['Google Pay', 'Tez', 'PhonePe', 'Paytm', 'BHIM', 'Amazon Pay',
                  'WhatsApp Pay', 'PayZapp', 'Airtel Payments Bank - Airtel Thanks App', 'Freecharge', 'MobiKwik',
                  'JioPay', 'Truecaller Pay', 'ICICI iMobile Pay', 'Axis Pay', 'HDFC Bank MobileBanking App', 'Samsung Pay',
                  'Ola Money', 'Uber Pay', 'Yes Pay', 'Federal Bank - FedMobile', 'Pockets by ICICI Bank', 'SBI Pay', 'Kotak - KayPay',
                  'IDFC First Bank - UPI App', 'Canara Bank - BHIM SBI Pay', 'Union Bank - Union Bank UPI App', 'IndusPay',
                  'Allahabad Bank - UPI App', 'Punjab National Bank - PNB UPI', 'Bank of Baroda - Baroda MPay', 'Central Bank - Cent UPI',
                  'Indian Bank - IndPay', 'Vijaya Bank - Vijaya UPI', 'Karnataka Bank - KBL SMARTz', 'UCO Bank - UCO UPI', 'Karur Vysya Bank - KVB Upay',
                  'Tamilnad Mercantile Bank - TMB UPI', 'Dhanlaxmi Bank - Dhanlaxmi UPI', 'Standard Chartered Bank - SC UPI', 'RBL Bank - RBL Pay']


bank_name = ['Bank of Baroda', 'Bank of India', 'Bank of Maharashtra', 'Canara Bank', 'Central Bank of India', 'Indian Bank', 'Indian Overseas Bank',
             'Punjab & Sind Bank', 'Punjab National Bank', 'State Bank of India', 'UCO Bank', 'Union Bank of India', 'Axis Bank', 'Bandhan Bank',
             'CSB Bank', 'City Union Bank', 'DCB Bank', 'Dhanlaxmi Bank', 'Federal Bank', 'HDFC Bank', 'ICICI Bank', 'Induslnd Bank', 'IDFC First Bank',
             'Jammu & Kashmir Bank', 'Karnataka Bank', 'Karur Vysya Bank', 'Kotak Mahindra Bank', 'Nainital Bank', 'RBL Bank', 'South Indian Bank',
             'Tamilnad Mercantile Bank', 'YES Bank', 'IDBI Bank', 'Au Small Finance Bank', 'Capital Small Finance Bank', 'Equitas Small Finance Bank',
             'Suryoday Small Finance Bank', 'Ujjivan Small Finance Bank', 'Utkarsh Small Finance Bank', 'ESAF Small Finance Bank', 'Fincare Small Finance Bank',
             'Jana Small Finance Bank', 'North East Small Finance Bank', 'Shivalik Small Finance Bank', 'Unity Small Finance Bank', 'India Post Payments Bank',
             'Fino Payments Bank', 'Paytm Payments Bank', 'Airtel Payments Bank', 'Andhra Pragathi Grameena Bank', 'Andhra Pradesh Grameena Vikas Bank',
             'Arunachal Pradesh Rural Bank', 'Aryavart Bank', 'Assam Gramin Vikash Bank', 'Bangiya Gramin Vikas Bank', 'Baroda Gujarat Gramin Bank',
             'Baroda Rajasthan Kshetriya Gramin Bank', 'Baroda UP Bank', 'Chaitanya Godavari Grameena Bank', 'Chhattisgarh Rajya Gramin Bank', 'Dakshin Bihar Gramin Bank',
             'Ellaquai Dehati Bank', 'Himachal Pradesh Gramin Bank', 'J&K Grameen Bank', 'Jharkhand Rajya Gramin Bank', 'Karnataka Gramin Bank', 'Karnataka Vikas Grameena Bank',
             'Kerala Gramin Bank', 'Madhya Pradesh Gramin Bank', 'Madhyanchal Gramin Bank', 'Maharashtra Gramin Bank', 'Manipur Rural Bank', 'Meghalaya Rural Bank',
             'Mizoram Rural Bank', 'Nagaland Rural Bank', 'Odisha Gramya Bank', 'Paschim Banga Gramin Bank', 'Prathama UP Gramin Bank', 'Puduvai Bharathiar Grama Bank',
             'Punjab Gramin Bank', 'Rajasthan Marudhara Gramin Bank', 'Saptagiri Grameena Bank', 'Sarva Haryana Gramin Bank', 'Saurashtra Gramin Bank', 'Tamil Nadu Grama Bank',
             'Telangana Grameena Bank', 'Tripura Gramin Bank', 'Utkal Grameen bank', 'Uttar Bihar Gramin Bank', 'Uttarakhand Gramin Bank', 'Uttarbanga Kshetriya Gramin Bank',
             'Vidharbha Konkan Gramin Bank', 'AB Bank', 'Abu Dhabi Commercial Bank', 'American Express Banking Corporation', 'Australia and New Zealand Banking Group',
             'Barclays Bank Plc.', 'Bank of America', 'Bank of Bahrain & Kuwait BSC', 'Bank of Ceylon', 'Bank of China', 'Bank of Nova Scotia', 'BNP Paribas', 'Citibank N.A.',
             'Cooperatieve Rabobank U.A.', 'Credit Agricole Corporate & Investment Bank', 'Credit Suisse A.G', 'CTBC Bank', 'DBS Bank India', 'Deutsche Bank',
             'Doha Bank Q.P.S.C', 'Emirates Bank NBD', 'First Abu Dhabi Bank PJSC', 'FirstRand Bank', 'HSBC', 'Industrial & Commercial Bank of China', 'Industrial Bank of Korea',
             'J.P. Morgan Chase Bank N.A.', 'JSC VTB Bank', 'KEB Hana Bank', 'Kookmin Bank', 'Krung Thai Bank Public Co.', 'Mashreq Bank PSC', 'Mizuho & Bank', 'MUFG Bank,',
             'NatWest Markets Plc', 'PT Bank Maybank Indonesia TBK', 'Qatar National Bank', 'Sberbank', 'SBM Bank (India)', 'Shinhan Bank', 'Societe Generale',
             'Sonali Bank', 'Standard Chartered Bank', 'Sumitomo Mitsui Banking Corporation', 'United Overseas Bank', 'Woori Bank', 'yesbank', 'sbi', 'hdfc', 'Equitas',"ppbl",
             "BOB", "Boi", " Bom ", " Cnb ", "Cbi", "Ib", "Iob", "Psb", " Pnb", " Ucob ", "Ubi", "Ab", "Bb", " Csbb ", "Cub", "Dcbb", "Dlb", "Fb", " Hdfcb", "Icicib",
             "Iib", " Idfcfb ", " Jkb ", " Kb ", " Kvb ", " Nb ", " Rblb ", " Sib ", " Tmb ", " Yesb ", " Idbi ", "Au small fb", " Equitas small fb ", "suryoday small fb", "ujjivan small fb",
             "utkarsh small fb", "Esaf small fb", "fincare small fb", "Jana small fb", "North east small fb", "Shivalik small fb", "Unitybsmall fb",
             "Ippb", "ppbl","kbl" , "Finopb", "Paytmpb", "Airtelpb", " IndusInd ","Apgb", "Apgvb", " Aprb ", " Ab ", " Agvb ", "Bgb", "Bggb", "Brkgb", "Bupb", "Cggb", "Crgb", "Dbgb"]


merchant = ['Myntra', 'Ajio', 'Lifestyle', 'Shoppers Stop', 'Central', 'Westside', 'Pantaloons', 'Tata Cliq', 'Amazon India Fashion', 'Flipkart Fashion', 'Koovs',
            'Jabong', 'Fabindia', 'H&M', 'Zara', 'Nykaa', 'Sephora India', 'The Body Shop India', 'Forest Essentials', 'Kama Ayurveda', 'Innisfree India',
            'Bath & Body Works India', 'MAC Cosmetics India', 'Fossil India', 'Swarovski India', 'Titan', 'Raymond', 'Allen Solly', 'Van Heusen',
            'Louis Philippe', 'Marks & Spencer', "Levi's", 'Nike', 'Adidas', 'Puma', 'Pepperfry', 'Urban Ladder', 'Home Centre', 'HomeTown', 'PepperTap', 'Grofers',
            'BigBasket', "Nature's Basket", "Spencer's Retail", 'Reliance Fresh', "Godrej Nature's Basket", 'Good Earth', 'Saravana Stores', 'Lifestyle International',
            'FabIndia Overseas', 'Cult.fit', "Gold's Gym India", 'Talwalkars', 'Decathlon India', 'Adidas India', 'Nike India', 'Reebok India', 'Puma India', 'Goqii',
            'HealthKart', 'Practo', 'Netmeds', 'Lenskart', 'Titan Eye Plus', 'CaratLane', 'BlueStone', 'Tanishq', 'Malabar Gold & Diamonds', 'Joyalukkas', 'PC Jeweller',
            'Kalyan Jewellers', 'Senco Gold & Diamonds', 'Voylla', 'Pipa Bella', 'Bluestone Jewellery & Lifestyle', 'FabAlley', 'Abof', 'Meesho', 'Netflix India',
            'Amazon Prime Video India', 'Disney+ Hotstar', 'Voot', 'ZEE5', 'SonyLIV', 'MX Player', 'Eros Now', 'ALT Balaji', 'Hoichoi', 'Hungama Play', 'JioCinema',
            'Airtel Xstream', 'VOOT Kids', 'Discovery+', 'ALTBalaji', 'TVF Play', 'Dice Media', 'Viu', 'Sun NXT', 'YuppTV', 'Gaana', 'Saavn', 'Wynk Music', 'Hungama Music',
            'JioSaavn', 'ShemarooMe', 'Zingaat', 'YouTube', 'ZengaTV', 'BoxTV', 'Spuul', 'Hooq', 'BigFlix', 'Hungama Movies', 'Mubi India', 'Simply South', 'Aha Video',
            'Voot Select', 'ShemarooMe Box Office', 'Amazon Music India', 'JioSaavn Pro', 'Apple Music', 'Hungama Artist Aloud', 'Idea Movies & TV', 'Tata Sky Mobile',
            'Airtel TV', 'SunNXT', 'Zee5 Premium', 'ALTBalaji Premium', 'Vodafone Play', 'Idea Music', 'Ditto TV', 'Eros Now Plus', 'Voot Kids Premium', 'Disney+ Hotstar VIP',
            'MX Player Pro', 'Practo', 'PharmEasy', '1mg', 'Medlife', 'Netmeds', 'Apollo Pharmacy', 'Portea Medical', 'MedPlus', 'mfine', 'DocsApp', 'Lybrate', 'HealthifyMe',
            'Healthians', 'CallHealth', 'Medibuddy', 'Practo Prime', 'Medlife Plus', 'Medibuddy Gold', 'Portea CARE Plan', 'mfine ONE', 'DocsApp Gold', 'Lybrate Gold',
            'Apollo 24/7', 'Cure.fit', 'Onelife', 'Myupchar', 'Onsurity', 'Zoylo', 'SastaSundar', 'Meddo', 'Docttocare', 'MedCords', 'Medibazaar', 'DocsApp Consult',
            'Zomato', 'Swiggy', 'Uber Eats', 'Foodpanda', 'Faasos', 'FreshMenu', 'Box8', 'InnerChef', 'Ovenstory Pizza', 'Behrouz Biryani', 'Chaayos', 'TeaBox', 'Chai Point',
            'The Good Bowl', 'RAW Pressery', 'Swiggy Genie', 'Zomato Market', 'Dunzo', 'BigBasket', 'Grofers', 'Licious', 'FreshToHome', "Spencer's", "Nature's Basket",
            'Amazon Pantry', 'Flipkart Supermart', 'Scootsy', 'Eat.fit', 'Sattviko', 'Rebel Foods (formerly known as Faasos)', 'Jumbotail', 'Country Delight', 'Milkbasket',
            'SuprDaily', 'Epigamia', 'Blue Tokai Coffee', 'Sleepy Owl Coffee', 'Chaipoint', 'Saffron Fix', 'Sula Vineyards', 'Grofers Kitchen', 'Vahdam Teas', 'True Elements',
            'Indian Oil Corporation Limited (IOCL)', 'Bharat Petroleum Corporation Limited (BPCL)', 'Hindustan Petroleum Corporation Limited (HPCL)',
            'Reliance Industries Limited (RIL)', 'Essar Oil Limited', 'Shell India', 'Total India', 'Adani Gas Limited', 'GAIL (India) Limited', 'Petronet LNG Limited',
            'Shell and BP Fuel', 'Indian Oil Adani Gas Private Limited', 'Nayara Energy Limited', 'Reliance BP Mobility Limited', 'Total Adani Fuels Marketing Private Limited',
            'GVR Retail Private Limited', 'Aditya Fuel Services Private Limited', 'Gogas Elite Fuel Private Limited', 'Choice Fuels India Private Limited',
            'Heuro India Energie Private Limited', 'BigBasket', 'Grofers', 'Amazon Pantry', 'Flipkart Supermart', 'Reliance Fresh Direct', "Nature's Basket",
            "Spencer's", 'DMart Ready', "Godrej Nature's Basket", 'JioMart', 'Milkbasket', 'ZopNow', 'GrocerMax', 'Bazaar Cart', 'Needs Supermarket', 'Easyday Club',
            'Kirana King', 'More Megastore', 'StarQuik', 'My247Market', 'Box8', 'Supr Daily', 'FarmEasy', 'HappyFresh', 'Dunzo', 'ShopKirana', 'FreshToHome', 'Eemli',
            'Farm2Kitchen', 'LocalBanya', 'Daily Ninja', 'JustMyRoots', 'Metro Cash & Carry', 'SRS Grocery', 'Kada.in', 'Grocito', 'Jiffstore', 'Veggies 24x7', 'MyGrahak',
            'Grocer App', 'Satvacart', 'BazaarCart', 'Grofers', "Nature's Basket", 'Amazon Pantry', 'Big Basket', 'Flipkart Supermart', 'Reliance Fresh Direct',
            "Spencer's Online", 'Tata Q', 'MakeMyTrip', 'Cleartrip', 'Yatra', 'Goibibo', 'EaseMyTrip', 'Expedia', 'Travelguru', 'OYO', 'RedBus', 'ixigo', 'Thomas Cook',
            'SOTC', 'TravelTriangle', 'Via', 'HappyEasyGo', 'CheapTicket', 'Booking.com', 'Agoda', 'Airbnb', 'Hostelworld', 'Travelocity India', 'Musafir.com',
            'Akbar Travels', 'MakeMyJourney', 'Tripoto', 'Trivago', 'Priceline', 'Traveloka', 'Travelstart', 'CheapOair', 'Expedia.co.in', 'Travelocity India', 'Hotels.com',
            'TripAdvisor', 'Travelguru', 'Cleartrip', 'HappyEasyGo', 'Ezeego1', 'Cox & Kings', 'Travel Tours']

shopping_brands = {
    "Myntra", "Ajio", "Lifestyle", "Shoppers Stop", "Central", "Westside",
    "Pantaloons", "Tata Cliq", "Amazon India Fashion", "Flipkart Fashion",
    "Koovs", "Jabong", "Fabindia", "H&M", "Zara", "Nykaa", "Sephora India",
    "The Body Shop India", "Forest Essentials", "Kama Ayurveda", "Innisfree India",
    "Bath & Body Works India", "MAC Cosmetics India", "Fossil India", "Swarovski India",
    "Titan", "Raymond", "Allen Solly", "Van Heusen", "Louis Philippe", "Marks & Spencer",
    "Levi's", "Nike", "Adidas", "Puma", "Pepperfry", "Urban Ladder", "Home Centre",
    "HomeTown", "PepperTap", "Grofers", "BigBasket", "Nature's Basket", "Spencer's Retail",
    "Reliance Fresh", "Godrej Nature's Basket", "Good Earth", "Saravana Stores",
    "Lifestyle International", "FabIndia Overseas", "Cult.fit", "Gold's Gym India",
    "Talwalkars", "Decathlon India", "Adidas India", "Nike India", "Reebok India",
    "Puma India", "Goqii", "HealthKart", "Practo", "Netmeds", "Lenskart", "Titan Eye Plus",
    "CaratLane", "BlueStone", "Tanishq", "Malabar Gold & Diamonds", "Joyalukkas",
    "PC Jeweller", "Kalyan Jewellers", "Senco Gold & Diamonds", "Voylla", "Pipa Bella",
    "Bluestone Jewellery & Lifestyle", "FabAlley", "Abof", "Meesho"}

entertainment_brands = {
    "Netflix India","Amazon Prime Video India", "Disney+ Hotstar", "Voot", "ZEE5", "SonyLIV", "MX Player",
    "Eros Now", "ALT Balaji", "Hoichoi", "Hungama Play", "JioCinema", "Airtel Xstream",
    "VOOT Kids", "Discovery+", "ALTBalaji", "TVF Play", "Dice Media", "Viu", "Sun NXT",
    "YuppTV", "Gaana", "Saavn", "Wynk Music", "Hungama Music", "JioSaavn", "ShemarooMe",
    "Zingaat", "YouTube", "ZengaTV", "BoxTV", "Spuul", "Hooq", "BigFlix", "Hungama Movies",
    "Mubi India", "Simply South", "Aha Video", "Voot Select", "ShemarooMe Box Office",
    "Amazon Music India", "JioSaavn Pro", "Apple Music", "Hungama Artist Aloud",
    "Idea Movies & TV", "Tata Sky Mobile", "Airtel TV", "SunNXT", "Zee5 Premium",
    "ALTBalaji Premium", "Vodafone Play", "Idea Music", "Ditto TV", "Eros Now Plus",
    "Voot Kids Premium", "Disney+ Hotstar VIP", "MX Player Pro", "HOTSTAR"}

health_brands = {
    "Practo", "PharmEasy","1mg", "Medlife", "Netmeds", "Apollo Pharmacy", "Portea Medical", "MedPlus", "mfine",
    "DocsApp", "Lybrate", "HealthifyMe", "Healthians", "CallHealth", "Medibuddy",
    "Practo Prime", "Medlife Plus", "Medibuddy Gold", "Portea CARE Plan", "mfine ONE",
    "DocsApp Gold", "Lybrate Gold", "Apollo 24/7", "Cure.fit", "Onelife", "Myupchar",
    "Onsurity", "Zoylo", "SastaSundar", "Meddo", "Docttocare", "MedCords", "Medibazaar",
    "DocsApp Consult"}

food_drinks_brands = {
    "Zomato", "Swiggy", "Uber Eats", "Foodpanda", "Faasos","FreshMenu", "Box8", 
    "InnerChef", "Ovenstory Pizza", "Behrouz Biryani", "Chaayos",
    "TeaBox", "Chai Point", "The Good Bowl", "RAW Pressery", "Swiggy Genie",
    "Zomato Market", "Dunzo", "BigBasket", "Grofers", "Licious", "FreshToHome",
    "Spencer's", "Nature's Basket", "Amazon Pantry", "Flipkart Supermart", "Scootsy",
    "Eat.fit", "Sattviko", "Rebel Foods (formerly known as Faasos)", "Jumbotail",
    "Country Delight", "Milkbasket", "SuprDaily", "Epigamia", "Blue Tokai Coffee",
    "Sleepy Owl Coffee", "Chaipoint", "Saffron Fix", "Sula Vineyards", "Grofers Kitchen",
    "Vahdam Teas", "True Elements", " Shop ", "UDUPI", "Veg", "Bar"}

fuel_brands = {
    "Indian Oil Corporation Limited (IOCL)","Bharat Petroleum Corporation Limited (BPCL)", 
    "Hindustan Petroleum Corporation Limited (HPCL)","Reliance Industries Limited (RIL)",
    "Essar Oil Limited", "Shell India", "Total India","Adani Gas Limited", 
    "GAIL (India) Limited", "Petronet LNG Limited", "Shell and BP Fuel",
    "Indian Oil Adani Gas Private Limited", "Nayara Energy Limited", 
    "Reliance BP Mobility Limited","Total Adani Fuels Marketing Private Limited", 
    "GVR Retail Private Limited","Aditya Fuel Services Private Limited", 
    "Gogas Elite Fuel Private Limited","Choice Fuels India Private Limited", 
    "Heuro India Energie Private Limited", "Fuel", "Petrol"}

groceries_brands = {
    "BigBasket", "Grofers", "Amazon Pantry", "Flipkart Supermart", "Reliance Fresh Direct",
    "Nature's Basket", "Spencer's", "DMart Ready", "Godrej Nature's Basket", "JioMart",
    "Milkbasket", "ZopNow", "GrocerMax", "Bazaar Cart", "Needs Supermarket", "Easyday Club",
    "Kirana King", "More Megastore", "StarQuik", "My247Market", "Box8", "Supr Daily",
    "FarmEasy", "HappyFresh", "Dunzo", "ShopKirana", "FreshToHome", "Eemli", "Farm2Kitchen",
    "LocalBanya", "Daily Ninja", "JustMyRoots", "Metro Cash & Carry", "SRS Grocery", 
    "Kada.in","Grocito", "Jiffstore", "Veggies 24x7", "MyGrahak", "Grocer App", "Satvacart",
    "BazaarCart","Grofers", "Nature's Basket", "Amazon Pantry", "Big Basket", 
    "Flipkart Supermart", "Reliance Fresh Direct", "Spencer's Online", "Tata Q"}

travel_brands = {
    "MakeMyTrip", "Cleartrip", "Yatra","Goibibo", "EaseMyTrip", "Expedia", "Travelguru", 
    "OYO", "RedBus", "ixigo", "Thomas Cook","SOTC", "TravelTriangle", "Via", "HappyEasyGo",
    "CheapTicket", "Booking.com", "Agoda","Airbnb", "Hostelworld", "Travelocity India", 
    "Musafir.com", "Akbar Travels", "MakeMyJourney","Tripoto", "Trivago", "Priceline", 
    "Traveloka", "Travelstart", "CheapOair", "Expedia.co.in","Travelocity India", 
    "Hotels.com", "TripAdvisor", "Travelguru", "Cleartrip", "HappyEasyGo",
    "Ezeego1", "Cox & Kings", "Travel Tours", "Metro"}



